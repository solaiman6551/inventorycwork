from django.contrib import admin
from django.urls import path, include
from .router import router

urlpatterns = [
    path('admin/', admin.site.urls),
    path('item/', include('v1.urls')),
    path('api/', include(router.urls))
]
