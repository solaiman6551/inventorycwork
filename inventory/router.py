from v1.viewsets import ItemsViewset
from rest_framework import routers

router = routers.DefaultRouter()
router.register('item', ItemsViewset)