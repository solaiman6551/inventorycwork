from django.shortcuts import render, redirect
from .forms import ItemsForm
from .models import Items

# Create your views here.


def item_list(request):
    context = {'item_list': Items.objects.all()}
    return render(request, "item_add/item_list.html", context)


def item_form(request, id=0):
    if request.method == "GET":
        if id == 0:
            form = ItemsForm()
        else:
            item = Items.objects.get(pk=id)
            form = ItemsForm(instance=item)
        return render(request, "item_add/item_form.html", {'form': form})
    else:
        if id == 0:
            form = ItemsForm(request.POST)
        else:
            item = Items.objects.get(pk=id)
            form = ItemsForm(request.POST,instance= item)
        if form.is_valid():
            form.save()
        return redirect('/item/list')


def item_delete(request,id):
    item = Items.objects.get(pk=id)
    item.delete()
    return redirect('/item/list')
