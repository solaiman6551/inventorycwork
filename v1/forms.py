from django import forms
from .models import Items


class ItemsForm(forms.ModelForm):

    class Meta:
        model = Items
        fields = ('itemName','price','category')
        labels = {
            'itemName': 'Item Name'
        }

    def __init__(self, *args, **kwargs):
        super(ItemsForm, self).__init__(*args, **kwargs)
        self.fields['category'].empty_label = "Select"
