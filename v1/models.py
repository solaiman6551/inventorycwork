from django.db import models


class Category(models.Model):
    title = models.CharField(max_length=50)

    def __str__(self):
        return self.title

class Items(models.Model):
    itemName = models.CharField(max_length=100)
    price = models.CharField(max_length=15)
    category = models.ForeignKey(Category,on_delete=models.CASCADE)
