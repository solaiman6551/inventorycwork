from rest_framework import viewsets
from . import models
from . import serializers


class ItemsViewset(viewsets.ModelViewSet):
    queryset = models.Items.objects.all()
    serializer_class = serializers.ItemsSerializer
