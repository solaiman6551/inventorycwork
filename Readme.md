1#Create a simple inventory project in Django with any two products you like.
Answer: Inventory Project created with very simple interface. I just used a very simple form html template in this project.

2#Host the Django project in Heroku.
Answer: Django project hosted in Heroku successfully. 

Project Link: http://solaimaninventory.herokuapp.com/
Add Item Link: http://solaimaninventory.herokuapp.com/item/
Item List Link: http://solaimaninventory.herokuapp.com/item/list/ (Delete/Edit)


3#Create a simple Angular Material or Vue.js project and access the inventory in REST API. No need for user Authentication. Just make it simple.
Answer: I don’t know angular/vue.js that much! You can access the inventory in REST API

API Link: http://solaimaninventory.herokuapp.com/api/

GET: http://solaimaninventory.herokuapp.com/api/item/

POST: http://solaimaninventory.herokuapp.com/api/item/
Example: 
    {
        "itemName": "HP InkJet",
        "price": "15000",
        "category": 2
     }

PUT: http://solaimaninventory.herokuapp.com/api/item/{Product_ID}/ 
Example: 
Example_URL: http://solaimaninventory.herokuapp.com/api/item/4/ 
                 {
                     "id": 4,
                     "itemName": "Canon",
                     "price": "12000",
                     "category": 2
                 }

 
DELETE: http://solaimaninventory.herokuapp.com/api/item/{Product_ID}/ 
Example: 
Example_URL: http://solaimaninventory.herokuapp.com/api/item/2/ 

7#Write a description of how you can modify the "rasa-master" to connect it with your Django inventory.
Answer: I have tried to install the Rasa and integrate it with the Django but unfortunately there’s some error. What I have found that I can integrate the Rasa with Django adding the required files in the Django project directory. After that render the chatbot html file in views file.


8#Write what is Docker and how it works in Heroku.
Answer: Docker is a platform that uses a container to wrap all the code/projects and keep it as it is. I am avoiding all the traditional definition and giving an example of docker. 
Example: When we work on a project, we need to install many packages/dependencies in our machine. But the problem comes when we deliver the project to another machine or deploy the application to the server. This occurs due to some missing packages/dependencies or might be another one. But the project is working fine in my machine which I used for development. To avoid this kind of situation Docker offers us a very useful solution which uses a container to wrap all the code and it can be moved to any machine without having any error. Also, we can use the container to easily deploy the project/application to the server. 
How it works on Heroku?
Answer: Just create a profile on Heroku and push the container to the Heroku profile. From my point of view as we know server is a machine just as a localhost machine. Heroku is hosting our application just as the server and its just a machine. Using docker we are transferring/deploying our project/application to another machine and it works as this way. 



9#Write what do you think about micro-service architecture.
Answer: Microservice architecture is an architectural structure of an application as a collection of small autonomous of services. What I understand from my knowledge is that this architectural style has different module of a program. To handle the complexity of modern software, the microservices architecture makes it easy for the industry. The best part of this service is the independency. Every microservice can be deployed independently and as a result the number of dependency issues are much lower in that (Compared to the Monolithic Architecture). Though the architectural design is much complex in this but the development speed is higher. 
